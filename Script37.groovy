/** Iterating */
def family = [dad:"John", mom:"Jane"]

/** Using the default 'it' variable */
family.each{println it}

/** Getting the key and value from 'it' */
family.each{println "${it.value} is the ${it.key}" }

/** Using named variables for the key and value */
family.each{k,v ->
    println "{v} is the ${k}"
	}
