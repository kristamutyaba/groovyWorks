/** groovy currying parameters(curry() - used to preload values into the parameter) */
def calculateTax = { taxRate, amount ->
   return amount + (taxRate * amount)
   }
   
   def tax = calculateTax.curry(0.1)
   [10, 20, 30].each{
      println "Total cost: ${tax(it)}"
	  }
	 
