/** Using the groovy Range class to find ranges of dates */
def today = new Date()
def nextWeek = today + 7
(today..nextWeek).each{println it}