/** findAll() method: it helps you to return a new list that contains all the elements that match your criteria*/
def languages = ["Java", "Groovy", "PHP", "HTML", "CSS"]
println languages.findAll{ it.startsWith("P")}

   