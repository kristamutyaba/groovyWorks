/** max(), min() and sum() methods */
def scores = [80, 90, 70]
println "Max: "+scores.max() // max() returns the highest value
println "Min: "+scores.min() // min() returns the lowest value
println "Sum: "+scores.sum() // sum() adds up all the elements in the list 
   