/** Finding values in a Map using the values()  method:returns a list of all the values & containsValue() lets you know whether a value exists */
def family = [dad:"John", mom:"Jane"]
println family.values()
println family.containsValue("John")
  
  