/** Using the groovy contains() method  */
def r = 1..3
println r.contains(1) && r.contains(3)
println r.contains(2)
println r.contains(12)
