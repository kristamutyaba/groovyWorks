/** Mixing GStrings(String concatenators) and heredocs (Multiple string containers)*/
def name = "Krista"
def date = new Date()
def amount = 987.65
def template = """
  Dear ${name},
      This is a friendly notice that ${amount} was
	   deposited in your checking account on ${date}.
	   """
println "Hello ${template}"