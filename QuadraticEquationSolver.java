/** 
* Decription: Quadratic equations solver(Qaudratic equation is an equation that employs the variable x having the general form ax2 + bx + c = 0, where a, b and c are constants
 *                 and a does not equal zero; i.e the variable is squared but raised to no higher power) 
* Program: QuadraticEquationSolver.java
*/
import java.util.Scanner; // import scanner class to enable creation of Scanner object that is used to enable user input

public class QuadraticEquationSolver{
    /** Method main */
	public static void main(String[] args){
	    /** Create Scanner object to enable user input at the console */
		Scanner input = new Scanner(System.in);
		/** Prompt for user input */
		System.out.print("Enter a: ");
		double a = input.nextDouble();
		System.out.print("Enter b: ");
		double b = input.nextDouble();
		System.out.print("Enter c: ");
		double c = input.nextDouble();
		
		double discriminant = b * b - 4 * a * c;
		/** Determine the result based on the discriminnt */
		if(discriminant == ((b * b) - (4 * a) * c)){
		   double r1 = (-b + Math.pow(discriminant, 0.5)) / (2.0 * a);
		   double r2 = (-b  - Math.pow(discriminant, 0.5)) / (2.0 * a);
		  System.out.print("The roots are: " + r1 + " and " + r2);
	}
	else if(discriminant == 0){
	       double r1 = -b  / (2.0 * a);
	      System.out.print("The root is: " + r1);
	}
	else{
	  System.out.print("The equation has no real roots!");
}
}
}
