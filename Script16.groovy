/** Iterating in groovy */
def languages = ["Java", "Groovy", "JRuby"]

/** Using the default 'it' variable */
languages.each{println it}

/** Usin the named variable of your choice */
//languages.each{lang > 
  // println lang
   //}