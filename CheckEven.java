/**
  * Description: A java program that checks whether a number is even 
  * Program:    CheckEvent.java
  */
  import java.util.*;
  
  public class CheckEven{
   /** Method main */
     public static void main(String[] args){
	     /** Create Scanner object to enable user input */
		 Scanner input = new Scanner(System.in);
		 
		 /** Prompt for user input */
		 System.out.print("Enter an interger: ");
		 int n = input.nextInt();
		 
		 
		 /** Check whether a number is even or not */
		 System.out.print("Is " + n + " an even number?");
		 
		 if (n %2 == 0){
		    System.out.print(" true");
	   }
	   else{
	     System.out.print(" false");
	  }
	  }
	  }
	  