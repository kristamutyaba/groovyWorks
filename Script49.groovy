/** Advanced example of a closure */
def convertToCelcius = {
  return (5.0/9.0) * (it.toFloat() - 32.0)
  }
  
  [0, 32, 70, 100].each{
    println "${it} degrees fahrenheit in celcius: ${convertToCelcius(it)}"
	}
