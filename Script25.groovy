/** join() returns a String containing each element hence if you pass a string argument into join(), each element will be separated by the string*/
def languages = ["Java", "Groovy", "PHP", "HTML", "CSS"]
println languages.join()
println languages.join(",")
println languages.join("&")
   