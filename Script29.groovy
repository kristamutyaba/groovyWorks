/** flatten() - returns a single dimensional array from a multidimensional list */
def languages = ["Java", "Groovy"]
def others = ["HTML", "JavaScript", "CSS"]
println languages << others
languages = languages.flatten()
println languages
   