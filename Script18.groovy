/** Iterating with an index */
// eachWithIndex() gives you both the current element and a counter variable
def languages = ["Java", "Groovy", "JRuby"]
languages.eachWithIndex{lang, i ->
   println "{i}: ${lang}"
   }