/** Groovy dynamic typing(using a declared type with more one data type) */
class DynamicTyping{
  static void main(String[] args){
     def x = "Krista is"
	 println(x)
	 x = 24
	 println x
	 println x = 25
	 }
	}