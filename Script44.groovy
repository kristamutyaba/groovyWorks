/** Using the groovy for statement */
for(i in 1..3){ println "Attempt ${i}" }
println "\n"
(1..3).each{ println "Attempt ${it}" }
