/** Iterating */
def family = [dad:"John", mom:"Jane"]

/** Using the default 'it' variable */
family.each{println it}
