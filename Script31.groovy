/** spread-dot(*.) operator allows you to concisely iterate over a list */
def languages = ["Java", "Groovy", "CSS", "HTML", "JavaScript"]
println languages*.toUpperCase()